package main

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"os"

	"io/ioutil"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type Manifest struct {
	JS  string `json:"/dist/js/app.js"`
	CSS string `json:"/dist/css/app.css"`
}

var p = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	file, e := ioutil.ReadFile("./mix-manifest.json")
	if e != nil {
		os.Exit(1)
	}

	var m Manifest
	json.Unmarshal(file, &m)

	t, _ := template.ParseFiles("./resources/views/index.html")
	t.Execute(w, m)
})

func main() {
	r := mux.NewRouter()
	r.PathPrefix("/dist/").Handler(http.StripPrefix("/dist/", http.FileServer(http.Dir("./dist/"))))
	r.PathPrefix("/fonts/").Handler(http.StripPrefix("/fonts/", http.FileServer(http.Dir("./fonts/"))))
	r.Handle("/", p)
	err := http.ListenAndServe("127.0.0.1:8080", handlers.LoggingHandler(os.Stdout, r))
	log.Println(err)
}

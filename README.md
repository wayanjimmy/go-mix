## go-mix

Laravel [mix](https://github.com/JeffreyWay/laravel-mix) is awesome, let's use it in our Go project.

### How It Works ?
* Bundle js, css using Laravel mix

### Install
Clone the repo and install depedencies:

```bash
git clone https://github.com/wayanjimmy/go-mix.git
cd go-mix
glide install
glide up
yarn
npm run dev # start developing! visit localhost:8080 to see your website!
npm run watch # watch CSS, JS compile them automatically
```

### Coding Notes
* Store assets in `resources/assets` similar with Laravel :)

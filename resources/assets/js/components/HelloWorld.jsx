import React, { Component } from 'react';

class HelloWorld extends Component {
  render() {
    return (
      <div className="alert alert-danger">
        Hello World
      </div>
    );
  }
}

export default HelloWorld;

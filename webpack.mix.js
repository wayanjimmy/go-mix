let mix = require('laravel-mix').mix;

mix
  .react('resources/assets/js/app.jsx', 'dist/js')
  .sass('resources/assets/sass/app.scss', 'dist/css')
  .version();
